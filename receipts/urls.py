from django.urls import path
from receipts.views import create_account, receipt_list, create_receipt, account_list, category_list, create_category

urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home"),
]